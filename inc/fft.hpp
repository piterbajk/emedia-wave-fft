#include <iostream>
#pragma once
#include <algorithm>
#include <vector>
#include <complex>

const double PI = 3.141592653589793238463;

typedef std::complex<double> Complex;
typedef std::vector<Complex> CVector;

namespace fft
{

template <typename Iter>
void cooley_tukey(Iter first, Iter last) {
  
  auto size = last - first;
  
  if (size >= 2) {
    // split the range, with even indices going in the first half,
    // and odd indices going in the last half.
    auto temp = CVector(size / 2);
    for (size_t i = 0; i < size / 2; ++i) {
      temp[i] = first[i * 2 + 1];
      first[i] = first[i * 2];
    }
    for (size_t i = 0; i < size / 2; ++i) {
      first[i + size / 2] = temp[i];
    }

    // recurse the splits and butterflies in each half of the range
    auto split = first + size / 2;
    cooley_tukey(first, split);
    cooley_tukey(split, last);

    // now combine each of those halves with the butterflies
    for (size_t k = 0; k < size / 2; ++k) {
      auto comp = std::exp(Complex(0, -2.0 * PI * k / size));

      auto& bottom = first[k];
      auto& top = first[k + size / 2];
      top = bottom - comp * top;
      bottom -= top - bottom;
    }
  }
}

void performAndPlot(waveFile& wav, long samples = 0)
{
  auto sample_rate = wav.getHeader().sample_rate;

  if(wav.getHeader().format_type!=1) 
  {
    throw std::invalid_argument("Format processing not supported.");
  }

  if(wav.getHeader().num_channels == 1)
  {
    auto single_channel = wav.getSoundData();
    if(samples != 0) 
    { 
      single_channel.resize(samples); 
    }

    fft::cooley_tukey(single_channel.begin(), single_channel.end());
    plotting::plotMono(single_channel, sample_rate);
  }
  else if(wav.getHeader().num_channels == 2)
  {
    auto stereo = wav.getSeparatedChannels();
    auto left = stereo.first;
    auto right = stereo.second;
    if(samples != 0) 
    { 
      right.resize(samples); 
      left.resize(samples); 
    }

    fft::cooley_tukey(left.begin(), left.end());  
    fft::cooley_tukey(right.begin(), right.end());
    
    plotting::plotStereo(left, right, sample_rate);
  }
}

}