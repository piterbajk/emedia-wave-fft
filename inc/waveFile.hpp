#pragma once
#include "wav_t.hpp"
#include "ciphering.hpp"
#include "utilities.hpp"
#include "rsa.hpp"
#include <boost/multiprecision/cpp_int.hpp>

using big_int = boost::multiprecision::cpp_int;

typedef std::complex<double> Complex;
typedef std::vector<Complex> CVector;
typedef std::vector<big_int> RSAVector;

#define DEBUG 0

class waveFile
{
private:
    wav_t header;
    CVector sound_data;
    CVector original_sound_data;
    std::string file_name;
    long alignedSize;

protected:
    void alignToPowerOfTwo();
    void readHeader(std::ifstream& wave_file);
    void readSoundData(std::ifstream& wave_file);
    bool saveHeader(std::ofstream& wave_file);

public:
    waveFile(const std::string& fname);

    wav_t getHeader();
    CVector getSoundData();
    std::pair<CVector, CVector> getSeparatedChannels();

    void encodeDecodeXOR(int32_t);
    bool encryptRSA(std::string filenameToSaveKeys, std::string encryptedFileName);
    bool decryptRSA(std::string filenameToReadKeys, std::string decryptedFileName);
    
    void printHeader();
    void printAdditionalInfo();
    void printFileInfo(std::ifstream&);

    bool save(std::string);
    bool saveEncrypted(RSAVector rsaVector, std::string);
    bool saveDecrypted(RSAVector rsaVector, std::string);
};

// Definitions

waveFile::waveFile(const std::string& fname) : file_name(fname) 
    {
        std::ifstream myfile (fname, std::ios::binary);
        
        printFileInfo(myfile);
        readHeader(myfile);
        readSoundData(myfile);
        
        myfile.close();
        
        std::copy(sound_data.begin(), sound_data.end(), std::back_inserter(original_sound_data));
        alignToPowerOfTwo();
    }

wav_t waveFile::getHeader() { return header; }

CVector waveFile::getSoundData() { return sound_data; }

void waveFile::readHeader(std::ifstream& wave_file)
{
  //RIFF chunk
  wave_file.read(reinterpret_cast<char*>(header.riff_marker.data), header.riff_marker.size);
  wave_file.read(reinterpret_cast<char*>(&header.overall_size), 4);
  wave_file.read(reinterpret_cast<char*>(header.format.data), header.format.size);
  
  //FMT chunk
  wave_file.read(reinterpret_cast<char*>(header.fmt_marker.data), header.fmt_marker.size);
  wave_file.read(reinterpret_cast<char*>(&header.fmt_chunk_size), 4);
  wave_file.read(reinterpret_cast<char*>(&header.format_type), 2);
  wave_file.read(reinterpret_cast<char*>(&header.num_channels), 2);
  wave_file.read(reinterpret_cast<char*>(&header.sample_rate), 4);
  wave_file.read(reinterpret_cast<char*>(&header.byte_rate), 4);
  wave_file.read(reinterpret_cast<char*>(&header.block_align), 2);
  wave_file.read(reinterpret_cast<char*>(&header.bits_per_sample), 2);
  
  //DATA chunk
  wave_file.read(reinterpret_cast<char*>(header.data_marker.data), header.data_marker.size);
  wave_file.read(reinterpret_cast<char*>(&header.data_chunk_size), 4);
}

void waveFile::readSoundData(std::ifstream& wave_file)
  {
  size_t size = header.data_chunk_size;
  int bits_per_sample = header.bits_per_sample;

  if(bits_per_sample == 8)
  {
    int8_t data = 0;

    for(int pos=0; pos<size; pos++)
    {
      wave_file.read(reinterpret_cast<char*>(&data), 1);
      sound_data.emplace_back(data, 0);
    }
  }
  else if(bits_per_sample == 16)
  {
    int16_t data = 0;

    for(int pos=0; pos<size; pos++)
    {
      wave_file.read(reinterpret_cast<char*>(&data), 2);
      sound_data.emplace_back(data, 0);
      #if DEBUG
      std::cout << std::hex << data << " ";
      #endif
    }
    std::cout << std::dec << std::endl;
  }
  else if(bits_per_sample == 32)
  {
    int32_t data = 0;

    for(int pos=0; pos<size; pos++)
    {
      wave_file.read(reinterpret_cast<char*>(&data), 4);
      sound_data.emplace_back(data, 0);
    }
  } 
  else 
     throw std::logic_error("Unsupported bitrate");
}

void waveFile::alignToPowerOfTwo()
{
  auto size = sound_data.size();
  if(size && !(size&(size-1)))
  {
    // Already power of two
    alignedSize = size;
  }
  else
  {
    auto nextPowOfTwo = std::pow(2, std::ceil(std::log2(size)));
    sound_data.resize(nextPowOfTwo, Complex(0,0));
    alignedSize = nextPowOfTwo;
  }
}

void waveFile::printFileInfo(std::ifstream& wave_file){

  // get size of file
  wave_file.seekg(0, wave_file.end);
  int fsize = wave_file.tellg();
  wave_file.seekg(0, wave_file.beg);

  if(wave_file)
  {
    std::cout << "Read successful\n";
    std::cout << std::setw(20) << std::right << "File: " << file_name << '\n';
    std::cout << std::setw(20) << std::right << "Size: " << fsize/1024 << " Kb\n";
  }
  else
    std::cout << "Error: only " << wave_file.gcount() << "could be read";

}

std::pair<CVector, CVector> waveFile::getSeparatedChannels()
{
  CVector left_channel, right_channel;
  static int idx = 0;

  std::for_each(sound_data.begin(), sound_data.end(), [&](Complex elem)
  {
    if(idx % 2 == 0)
      left_channel.push_back(elem);
    else
      right_channel.push_back(elem);
    
    idx++;
  });

  return std::make_pair(left_channel, right_channel);
}

void waveFile::printHeader()
{
    std::cout << "\nFile HEADER data\n" \
                << std::setw(20) << std::right << "RIFF marker: " << header.riff_marker << '\n' \
                << std::setw(20) << std::right << "Overall size: " << header.overall_size << '\n' \
                << std::setw(20) << std::right << "Format: "<< header.format << '\n' \
                << std::setw(20) << std::right << "FMT marker: " << header.fmt_marker << '\n' \
                << std::setw(20) << std::right << "FMT chunk size: " << header.fmt_chunk_size << '\n' \
                << std::setw(20) << std::right << "Format type: " << (header.format_type==1?"PCM":header.format_type==6?"A-law":header.format_type==7?"Mu-law":"") << '\n' \
                << std::setw(20) << std::right << "No channels: " << (header.num_channels==1?"Mono":header.num_channels==2?"Stereo":std::to_string(header.num_channels)) << '\n' \
                << std::setw(20) << std::right << "Sample rate: " << header.sample_rate << " Hz\n" \
                << std::setw(20) << std::right << "Byte rate: " << header.byte_rate << " Bytes/s" << '\n' \
                << std::setw(20) << std::right << "Block align: " << header.block_align << " Bytes\n" \
                << std::setw(20) << std::right << "Bits per sample: " << header.bits_per_sample << " bits\n"
                << std::setw(20) << std::right << "DATA marker: " << header.data_marker << '\n' \
                << std::setw(20) << std::right << "DATA chunk size: " << header.data_chunk_size << '\n';
    }

void waveFile::printAdditionalInfo()
{
    int sample_count = (8*header.data_chunk_size)/(header.num_channels*header.bits_per_sample);
    double fft_bins = 0.5*header.sample_rate; //Nyquist freq

    std::cout << "\nAdditional data\n" \
                << std::setw(20) << std::right << "File data count: " << header.data_chunk_size << '\n' \
                << std::setw(20) << std::right << "Pow of 2 aligned: " << alignedSize << '\n' \
                << std::setw(20) << std::right << "Duration: " <<  sample_count/header.sample_rate << " s \n" \
                << std::setw(20) << std::right << "Sample size: " << header.bits_per_sample/8 << " Byte(s)\n" \
                << std::setw(20) << std::right << "Samples count: " << sample_count << '\n' \
                << std::setw(20) << std::right << "Frequency range: " << "0 - " << header.sample_rate/2 << " Hz\n"
                << std::setw(20) << std::right << "Resolution: " << (fft_bins/sample_count) << " Hz/bin" \
                << "\n\n";
}

bool waveFile::saveHeader(std::ofstream& wave_file)
{
  if(wave_file.is_open())
  {
      //RIFF chunk
    wave_file.write(reinterpret_cast<char*>(header.riff_marker.data), header.riff_marker.size);
    wave_file.write(reinterpret_cast<char*>(&header.overall_size), 4);
    wave_file.write(reinterpret_cast<char*>(header.format.data), header.format.size);
    
    //FMT chunk
    wave_file.write(reinterpret_cast<char*>(header.fmt_marker.data), header.fmt_marker.size);
    wave_file.write(reinterpret_cast<char*>(&header.fmt_chunk_size), 4);
    wave_file.write(reinterpret_cast<char*>(&header.format_type), 2);
    wave_file.write(reinterpret_cast<char*>(&header.num_channels), 2);
    wave_file.write(reinterpret_cast<char*>(&header.sample_rate), 4);
    wave_file.write(reinterpret_cast<char*>(&header.byte_rate), 4);
    wave_file.write(reinterpret_cast<char*>(&header.block_align), 2);
    wave_file.write(reinterpret_cast<char*>(&header.bits_per_sample), 2);

    //DATA chunk
    wave_file.write(reinterpret_cast<char*>(header.data_marker.data), header.data_marker.size);
    wave_file.write(reinterpret_cast<char*>(&header.data_chunk_size), 4);
    return true;
  }
  return false;
}

bool waveFile::save(std::string fname)
{
  std::ofstream wave_file(fname, std::ios::binary);
  saveHeader(wave_file);

  if(wave_file.is_open())
  {
    uint8_t bytes_per_sample = header.bits_per_sample/8;

    for(auto i = 0; i<original_sound_data.size()/2; ++i)
    {
      long value = std::real(original_sound_data[i]);
      wave_file.write(reinterpret_cast<const char*>(&value), bytes_per_sample);
    }
    
    wave_file.close();
    
    return true;
  }
  return false;
}

bool waveFile::saveEncrypted(std::vector<big_int> rsaVector, std::string fname)
{
  std::ofstream wave_file(fname, std::ios::binary);
  saveHeader(wave_file);

  if(wave_file.is_open())
  {
    //uint8_t bytes_per_sample = header.bits_per_sample/8;
    unsigned bytes_per_sample = 128;

    for(auto i = 0; i<rsaVector.size(); ++i)
    {
      mp::uint1024_t value = static_cast<mp::uint1024_t>(rsaVector[i]);
      wave_file.write(reinterpret_cast<const char*>(&value), bytes_per_sample);
      #if DEBUG
      std::cout << "SavedE: " << std::hex << value << '\n';
      #endif
    }
    
    wave_file.close();
    
    return true;
  }
  return false;
}

bool waveFile::saveDecrypted(std::vector<big_int> rsaVector, std::string fname)
{
  std::ofstream wave_file(fname, std::ios::binary);
  saveHeader(wave_file);

  if(wave_file.is_open())
  {
    for(auto i = 0; i<rsaVector.size()/2; ++i)
    {
      mp::uint1024_t value = static_cast<mp::uint1024_t>(rsaVector[i]);      
      mp::uint1024_t changed = static_cast<mp::uint1024_t>(utilities::changeEndianess(rsaVector[i], 127, 2));
      wave_file.write(reinterpret_cast<const char*>(&changed), 128-2);
      #if DEBUG
      std::cout << "SavedD: " << std::hex << value << '\n';
      std::cout << "EndiaD: " << std::hex << changed << '\n';
      #endif
    }
    
    wave_file.close();
    
    return true;
  }
  return false;
}

void waveFile::encodeDecodeXOR(int32_t key)
{
  ciphering::performXOR(original_sound_data, key);
}

bool waveFile::encryptRSA(std::string filenameToSaveKeys, std::string encryptedFileName)
{
    RSA rsa{};
    std::vector<uint16_t> dataToEncrypt{0};
    std::transform(original_sound_data.begin(), original_sound_data.end(), std::back_inserter(dataToEncrypt),[](auto elem){
      return std::real(elem);
      });
    RSAVector encryptedVector = rsa.encryptVector(dataToEncrypt);
    rsa.saveKeys(filenameToSaveKeys);
    saveEncrypted(encryptedVector, encryptedFileName);
}

bool waveFile::decryptRSA(std::string filenameToReadKeys, std::string decryptedFileName)
{
  RSA rsa{};
    std::vector<uint16_t> dataToDecrypt{0};
    std::transform(original_sound_data.begin(), original_sound_data.end(), std::back_inserter(dataToDecrypt),[](auto elem){
      return std::real(elem);
      });
  rsa.loadKeys(filenameToReadKeys);
  RSAVector decryptedVector = rsa.decryptVector(dataToDecrypt);
  saveDecrypted(decryptedVector, decryptedFileName);
}