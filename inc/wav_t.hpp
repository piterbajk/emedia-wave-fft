#pragma once
#include<iostream>
#include<fstream>
#include<string>
#include<vector>
#include<iomanip>
#include<complex>
#include<cmath>
#include<algorithm>
#include<exception>

/*
  Program works correctly on machines with Little Endian byte order
*/

struct big_endian_form_t
{
    char data[5];
    const unsigned size = 4;

    big_endian_form_t(){ data[4]='\0'; }
    inline char& operator [](int idx){ return data[idx]; }
    inline friend std::ostream& operator <<(std::ostream& os, const big_endian_form_t& obj){ os << obj.data; return os; }
};

struct wav_t
{
    // RIFF chunk descriptor
    big_endian_form_t riff_marker;
    uint32_t overall_size;
    big_endian_form_t format;

    // FMT sub-chunk
    big_endian_form_t fmt_marker;
    uint32_t fmt_chunk_size;
    uint16_t format_type;
    uint16_t num_channels;
    uint32_t sample_rate;
    uint32_t byte_rate;
    uint16_t block_align;
    uint16_t bits_per_sample;

    // DATA sub-chunk
    big_endian_form_t data_marker;
    uint32_t data_chunk_size;
};