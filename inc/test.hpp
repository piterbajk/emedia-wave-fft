#pragma once

void test()
{
  double a = 1;
  double f1 = 1;    
  double f2 = 0;
  double twoPiF1 = 2*PI*f1;
  double twoPiF2 = 2*PI*f2;
  
  double sampleRate = 8;
  double sampleCount = 8;
  
  CVector buff(sampleCount); 
  
  for(int sample=0; sample<buff.size(); ++sample)
  {
    double time = sample/sampleRate;
    buff[sample] = a*(sin(twoPiF1*time)+sin(twoPiF2*time));
  }

  fft::cooley_tukey(buff.begin(), buff.end());
  for(auto i = 0; i<buff.size()/2; ++i)
  {
    std::cout << i*sampleRate/sampleCount << ' ' << 2*std::abs(buff[i]) << '\n';
  }
}