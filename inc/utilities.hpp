namespace utilities{

template <typename T>
T changeEndianess(T value, int sizeBytes, int bytesPerSample)
{
  T result{};
  
  auto chunksCount = sizeBytes/bytesPerSample;
  auto bitshift = bytesPerSample * 8;
  
  for(auto i = 0; i < chunksCount; ++i)
  {
    result |= (value & 0xFFFF);
    if(i != chunksCount-1)
    {
      result <<= bitshift;
      value >>= bitshift;
    }
  }
  return result;
}

}