#pragma once
#include <fstream>
#include <vector>
#include <complex>
#include <cmath>
#include <sstream> 
#include <utility>
#include <algorithm>
#include <boost/multiprecision/cpp_int.hpp>
#include <boost/multiprecision/miller_rabin.hpp>
#include <boost/random/mersenne_twister.hpp>

namespace mp = boost::multiprecision;
using big_int = mp::cpp_int;

typedef std::complex<double> Complex;
typedef std::vector<Complex> CVector;
typedef std::vector<uint16_t> ByteVector;

#define DEBUG 0

class RSA
{
    private:
        big_int public_key;
        big_int private_key;
        big_int module;
        bool keys_generated;
        unsigned key_length;
        unsigned chunk_size;
        
        big_int gcd(big_int a, big_int b);
        bool isPrime(big_int n);
        big_int modInverse(big_int a, big_int m);
        big_int generatePrime();
        void generateKeys();
        big_int fastExpo(big_int base, big_int exp, big_int modulus);

    public:
        RSA();
        big_int encrypt(big_int& data);
        big_int decrypt(big_int& data);
        auto encryptVector(ByteVector& data);
        auto decryptVector(std::vector<uint16_t> & data);
        bool saveKeys(std::string filename);
        bool loadKeys(std::string filename);
};

RSA::RSA(): key_length(512), keys_generated(false) 
{
    chunk_size = key_length/8-1;
}

bool RSA::isPrime(big_int n)
{
    if(n==1) return false;
    if(n==2) return true;
    if(n%2==0) return false;

    big_int sqrt = mp::sqrt(n);
    
    for(big_int i=2; i<sqrt; ++i)
    {
        if(n%i==0) return false;
    }

    return true;
}

/* Greatest Common Divisor */
big_int RSA::gcd(big_int a, big_int b)
{
    if (a == 0)
        return b;
    return gcd(b%a, a);
}

big_int RSA::generatePrime()
{
    boost::random::mt11213b base_gen(clock());
    boost::random::independent_bits_engine<boost::random::mt11213b, 512, big_int> gen(base_gen);

    // Generate some large random primes
    // Note 25 trials of Miller-Rabin 
    // likelihood that number is prime
    big_int n;
    do
    {
        n = gen();
    }while (!miller_rabin_test(n, 25));
    
    return n;
}

big_int RSA::modInverse(big_int a, big_int m)
{
    big_int m0 = m;
    big_int y = 0, x = 1;

    if (m == 1)
    return 0;

    while (a > 1)
    {
        // q is quotient
        big_int q = a / m;
        big_int t = m;

        // m is remainder now, process same as
        // Euclid's algo
        m = a % m, a = t;
        t = y;

        // Update y and x
        y = x - q * y;
        x = t;
    }

    // Make x positive
    if (x < 0)
    x += m0;

    return x;
}

void RSA::generateKeys()
{
    big_int p{}, q{}, phi{}, n{}, e{}, d{};
    do
    {
        p = generatePrime(); 
        q = generatePrime();
    } while(p == q || !p || !q);

    std::cout << "generated " << p << " " << q <<"\n";

    phi = (p-1)*(q-1);
    n = p*q;

    for(e=3; gcd(e, phi) != 1 ; e+=2);
    d = modInverse(e, phi);

    std::cout << "Module:      " << n << '\n' \
              << "Public key:  " << e << '\n' \
              << "Private key: " << d << "\n";


    public_key = e;
    private_key = d;
    module = n;

    keys_generated = true;
}

big_int RSA::fastExpo(big_int base, big_int exp, big_int modulus)
{
	big_int result;
	base %= modulus;
	result = 1;
	while (exp > 0)
	{
		if (exp % 2 == 1)
			result = (result * base) % modulus;
		base = mp::pow(base, 2) % modulus;
		exp >>= 1;
	}

	return result;
}

big_int RSA::encrypt(big_int& data)
{ 
    if(!keys_generated)
        generateKeys();

    big_int encrypted = fastExpo(data, public_key, module);
    std::cout << std::fixed << encrypted << '\n';

    return encrypted;
}

big_int RSA::decrypt(big_int& data)
{ 
    big_int decrypted = fastExpo(data, private_key, module);
    std::cout << std::fixed << decrypted << '\n';
    return decrypted;
}

auto RSA::encryptVector(ByteVector & data)
{
    std::vector<big_int> encryptedVector;
    if(!keys_generated)
        generateKeys();
    
    auto chunkFront = data.begin(), chunkBack = data.begin();
    chunkBack += chunk_size;
    
    auto fullChunksCount = std::ceil(data.size()/chunk_size); 
    auto remainingBytes = data.size()%chunk_size;
    mp::uint1024_t chunkToEncrypt{0}, encryptedChunk{0};

    /* Full chunks encryption */
    for(auto i=0; i < fullChunksCount; ++i)
    {
        mp::import_bits(chunkToEncrypt, chunkFront, chunkBack);
        
        #if DEBUG
        std::cout << "Before shift " << std::dec << i << " : " << std::fixed << std::hex << chunkToEncrypt << '\n';
        #endif
        
        chunkToEncrypt <<= 8;

        #if DEBUG
        std::cout << "After shift  " << std::dec << i << " : " << std::fixed << std::hex << chunkToEncrypt << '\n';
        #endif

        encryptedChunk = static_cast<mp::uint1024_t>(fastExpo(chunkToEncrypt, public_key, module));
        
        #if DEBUG
        std::cout << "Encrypted    " << std::dec << i << " : " << std::fixed << std::hex << encryptedChunk << "\n\n";
        #endif

        encryptedVector.push_back(std::move(encryptedChunk));

        chunkFront += chunk_size; chunkBack += chunk_size;
    }

    /* Handle last - not full chunk encryption*/
    if(remainingBytes)
    {
        chunkBack = chunkFront+remainingBytes;
        mp::import_bits(chunkToEncrypt, chunkFront, chunkBack);
        
        auto zeroPaddingSize = (chunk_size - remainingBytes+1)*8;
        chunkToEncrypt <<= zeroPaddingSize;
        
        encryptedChunk = static_cast<mp::uint1024_t>(fastExpo(chunkToEncrypt, public_key, module));
        #if DEBUG
        std::cout << "LToEncrypt " << std::fixed << std::hex << chunkToEncrypt << '\n';
        std::cout << "LEncrypted " << std::fixed << std::hex << encryptedChunk << '\n';
        #endif
        encryptedVector.push_back(std::move(encryptedChunk));
    }

    return encryptedVector;
}

auto RSA::decryptVector(std::vector<uint16_t> & data)
{ 
    std::vector<big_int> decryptedVector;
    mp::uint1024_t chunkToDecrypt{0}, decryptedChunk{0};
    auto chunksCount = data.size()/chunk_size;
    auto chunkFront = data.begin()+1, chunkBack = data.begin()+1; // HERE
    auto chunkFixedSize = (chunk_size+1);
    chunkBack += chunkFixedSize;
    std::cout << "Chunk size: " << chunkFixedSize << '\n';
    
    for(auto i=0; i < chunksCount; ++i)
    {
        chunkToDecrypt = 0;
        std::reverse(chunkFront, chunkBack);
        mp::import_bits(chunkToDecrypt, chunkFront, chunkBack);
        #if DEBUG
        std::cout << "ToDecrypt " << std::dec << i << " : " << std::fixed << std::hex << chunkToDecrypt << '\n';
        #endif
        decryptedChunk = static_cast<mp::uint1024_t>(fastExpo(chunkToDecrypt, private_key, module));
        decryptedChunk >>= 8;
        #if DEBUG
        std::cout << "Decrypted " << std::dec << i << " : " << std::fixed << std::hex << decryptedChunk << '\n';
        #endif
        decryptedVector.push_back(std::move(decryptedChunk));
        chunkFront += chunkFixedSize; chunkBack += chunkFixedSize;
    }

    return decryptedVector;
}

bool RSA::saveKeys(std::string filename)
{
    std::ofstream outfile(filename, std::ofstream::out);
    if(!outfile.is_open()) return false;

    outfile << "Module: " << module << '\n' \
            << "Public key: " << public_key << '\n' \
            << "Private key: " << private_key;
            
    outfile.close();
    return true;
}

bool RSA::loadKeys(std::string filename)
{
    std::ifstream infile(filename, std::ifstream::in);    
    if(!infile.is_open()) return false;
    std::string tmp{};
    while(std::getline(infile, tmp))
    {
        big_int ll;
        auto pos = tmp.find(':')+1;
        std::istringstream iss(std::string(tmp.begin()+pos, tmp.end()));
        iss >> ll;
        if(tmp.substr(0, pos) == "Public key:")
            public_key = ll;
        else if(tmp.substr(0, pos) == "Private key:")
            private_key = ll;
        else if(tmp.substr(0, pos) == "Module:")
            module = ll;
    }

    if(!public_key || !private_key || !module)
    {
        std::cerr << "Invalid keys! Cannot load.\n";
        return false;
    }

    std::cout << "\n### RSA keys loaded from file ###\n" \
            << "Public key:  " << public_key << '\n' \
            << "Private key: " << private_key << '\n'
            << "Module:      " << module << '\n';

    infile.close();

    return true;
}