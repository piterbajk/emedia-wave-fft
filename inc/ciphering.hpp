#pragma once
#include <vector>
#include <complex>
#include <cmath>

typedef std::complex<double> Complex;
typedef std::vector<Complex> CVector;

namespace ciphering
{
/* XOR */

template <typename T>
T XOREncodeDecode(T value, T key)
{
    return value^key;
}

template <typename U>
void performXOR(CVector& data, U key)
{
    for(auto& elem : data)
    {
        int32_t value = static_cast<int32_t>(std::real(elem));
        double afterXOR = XOREncodeDecode(value, key);
        elem = Complex(afterXOR, 0);
    }
}

/***************************************************************/
}