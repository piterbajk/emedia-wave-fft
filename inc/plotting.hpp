#pragma once

namespace plotting{

static int PLOT_LIMIT_DN = 10;
static int PLOT_LIMIT_UP = 0;
static bool DECIBEL_SCALE = false;
static std::string GNUPLOT_STYLE = "impulses";

void set_freq_limits(int lower, int upper) try
{
    if(lower < upper)
    {
      PLOT_LIMIT_UP = upper;
      PLOT_LIMIT_DN = lower;
    }
    else
      throw std::logic_error("Incorrect limits");
}
catch(std::exception e)
{
    std::cout << e.what() << std::endl;
}

void plotStereo(CVector& fft_left, CVector& fft_right, double sampleRate) try
{
  auto size = fft_left.size();
  std::vector<double> freqs(size), magnL(size), magnR(size);
  
  for (int i = 0; i < size/2; i++)
  {
    freqs[i] = i*sampleRate/size;
    if(DECIBEL_SCALE)
    {
      magnL[i] = 20*std::log10(2*std::abs(fft_left[i]));
      magnR[i] = 20*std::log10(2*std::abs(fft_right[i]));
    }
    else
    {
      magnL[i] = 2*std::abs(fft_left[i]);
      magnR[i] = 2*std::abs(fft_right[i]);
    }
  }

  Gnuplot gl("Left channel FFT"), gr("Right channel FFT");
  if(DECIBEL_SCALE) GNUPLOT_STYLE = "lines"; 
  gl.set_title("Left channel FFT").set_xrange(PLOT_LIMIT_DN, PLOT_LIMIT_UP > 0 ? PLOT_LIMIT_UP : (sampleRate/2) ).set_style(GNUPLOT_STYLE);
  gl.set_grid().set_yautoscale().set_xlabel("Frequency [Hz]");
  gr.set_title("Right channel FFT").set_xrange(PLOT_LIMIT_DN, PLOT_LIMIT_UP > 0 ? PLOT_LIMIT_UP : (sampleRate/2) ).set_style(GNUPLOT_STYLE);
  gr.set_grid().set_yautoscale().set_xlabel("Frequency [Hz]");
  
  if(DECIBEL_SCALE) 
  {
    gl.set_ylabel("Magnitude [dB]");
    gr.set_ylabel("Magnitude [dB]"); 
  }
  else
  {
    gl.set_ylabel("Magnitude"); 
    gr.set_ylabel("Magnitude"); 
  }

  gl.plot_xy(freqs, magnL);
  gr.plot_xy(freqs, magnR);

  std::cout << "\nPress enter to exit...\n";
  std::cin.get();
}
catch(GnuplotException ge)
{
  std::cout << ge.what() << std::endl;
}

void plotMono(CVector& fft_data, double sampleRate) try
{
  auto size = fft_data.size();
  std::vector<double> freqs(size), magns(size);
  
  for (int i = 0; i < size/2; i++)
  {
    if(DECIBEL_SCALE)
    {
      freqs[i] = i*sampleRate/size;
      magns[i] = 20*std::log10(2*std::abs(fft_data[i]));
    }
    else
    {
      freqs[i] = i*sampleRate/size;
      magns[i] = 2*std::abs(fft_data[i]);
    }
  }

  Gnuplot g("FFT");
  if(DECIBEL_SCALE) GNUPLOT_STYLE = "lines"; 
  g.set_title("FFT").set_xrange(PLOT_LIMIT_DN, PLOT_LIMIT_UP?PLOT_LIMIT_UP:(sampleRate/2)).set_style(GNUPLOT_STYLE);
  g.set_grid().set_xlabel("Frequency [Hz]");
  
  if(DECIBEL_SCALE) 
    g.set_ylabel("Magnitude [dB]");   
  else
    g.set_ylabel("Magnitude"); 
  
  g.plot_xy(freqs, magns);

  std::cout << "\nPress enter to exit...\n";
  std::cin.get();
}
catch(GnuplotException ge)
{
  std::cout << ge.what() << std::endl;
}

}