#include <unistd.h>
#include "inc/gnuplot_i.hpp"
#include "inc/waveFile.hpp"
#include "inc/plotting.hpp"
#include "inc/fft.hpp"
#include "inc/test.hpp"
#include "inc/rsa.hpp"

int main(int argc, char *argv[])
{
  if (argc < 2)
  {
    std::cerr << "Incorrect args.\n\nUsage example:\n ./a.out 'file_path'\n\n";
    std::cerr << "\
Analyse: \n\
    -f         - perform FFT on all channels\n\
    -n <value> - process only N first samples\n\
    -l <value> - lower plot limit\n\
    -h <value> - upper plot limit\n\
    -b         - dB scale\n\
Cipher:\n\
    -x         - encrypt/decrypt using XOR (bidirectional)\n\
    -k <fname> - file to load/save RSA keys\n\
    -e         - encrypt using RSA\n\
    -d         - decrypt using RSA\n\
I/O operations: \n\
    -s <fname> - save processed WAVE to file\n"; 
    return -1;
  }

  int c = 0, lower = 0, upper = 0, samples = 0, key = 0;
  
  std::string filenameToStoreKeys("keys.txt");
  
  waveFile wave(argv[1]);
  wave.printHeader();

  while ((c = getopt (argc, argv, "bd:e:fh:k:l:n:s:vx:")) != -1)
    switch(c)
    {
    case 'b':
      plotting::DECIBEL_SCALE = true;
    break;
    case 'd':
    {
      std::string decryptedName("decrypted.wav");
      if(optarg) 
        decryptedName = std::string(optarg);
      wave.decryptRSA(filenameToStoreKeys, decryptedName);
    }
    break;
    case 'e':
    {
      std::string encryptedName("encrypted.wav");
      if(optarg) 
        encryptedName = std::string(optarg);
      wave.encryptRSA(filenameToStoreKeys, encryptedName);
    }
    break;
    case 'l':
      lower = std::atoi(optarg);
    break;
    case 'h':
      upper = std::atoi(optarg);
    break;
    case 'k':
      filenameToStoreKeys = std::string(optarg);
    break;
    case 'n':
      samples = std::atoi(optarg);
    break;
    case 's':
      if(optarg && wave.save(optarg))
        std::cout << "\nSuccessfully saved to file: " << optarg << '\n';
      else
        std::cerr << "\nSaving failed!\n";
    break;
    case 'x':
      key = std::atoi(optarg);
      wave.encodeDecodeXOR(key);
    break;
    case 'f':
      if(lower && upper)  plotting::set_freq_limits(lower, upper); 
      fft::performAndPlot(wave, samples);
    break;
    case 'v':
      wave.printAdditionalInfo();
    break;
  }

  return 0;
}