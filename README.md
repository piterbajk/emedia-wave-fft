# Program

Simple cross platform C++ program for reading WAVE files with RSA ciphering algorithm.  
  
The WAVE file structure:  
![alt text](http://soundfile.sapp.org/doc/WaveFormat/wav-sound-format.gif)

# Compile program 
`g++ -std=c++14 -o wavprocessor main.cpp`

# Run examples
## Simple file read to check wave header
`./wavprocessor samples/1kHz.wav`
## Plot FFT
`./wavprocessor samples/1kHz.wav -f`
## Encrypt with RSA
`./wavprocessor samples/1kHz.wav -e encrypted.wav`
## Decrypt with RSA
`./wavprocessor encrypted.wav -d decrypted.wav`

# Use flags for more options

## Analyse:  
    -f         - perform FFT on all channels  
    -n <value> - process only N first samples  
    -l <value> - lower plot limit  
    -h <value> - upper plot limit  
    -d         - dB scale  
## Cipher:  
    -x         - encrypt/decrypt using XOR (bidirectional)  
    -k <fname> - file to load/save RSA keys
    -e         - encrypt using RSA  
    -d         - decrypt using RSA  
## I/O operations:  
    -s <fname>  - save processed WAVE to file  


# Additional info

More audio files can be found in `samples/` directory.  
Default file to load/save RSA keys is `keys.txt`
